#include <gb/gb.h>
#include <stdint.h>

#include <stdio.h>

#include <gbdk/platform.h>
#include <gbdk/font.h>

#include <gbdk/console.h>

#include "lines.h"

void printPage(char offset) {
	char x = 0;
	cls();
	for (x = 0; x < 18; x++) {
		gotoxy(0, x);
		printf(lines[x + offset]);	
	}
}

void main(void)
{
    font_t ibm_font;
	font_init();
	ibm_font = font_load(font_ibm);
	font_set(ibm_font);
	font_set(ibm_font);

	int off = 0;

    while(1) {

		switch(joypad()) {
		
		case J_RIGHT :
			break;
		case J_LEFT :
			break;
		case J_UP :
			if (off > 0) off--;
			printPage(off);
			break;
		case J_DOWN :
			off++;
			printPage(off);
			break;
		case J_START :
			break;
		case J_SELECT :
			break;
		case J_A :
			break;
		case J_B :
			break;			
		default :
			break;
		}
		
		

        wait_vbl_done();
    }
}
